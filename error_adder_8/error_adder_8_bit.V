module fa(a, b, c, sum, carry,en);
input a;
input b;
input c;
input en;
output sum;
output carry;
wire en;
reg sum,carry,d,e,f;
always@(a,b,c,en)
begin
if(en==1'b1)
begin
sum=a^b^c;
 d=a&b;
 e=a|b;
 f=e&c;
carry=d|f;
end
else
begin
sum=0;
carry=0;
end
end
endmodule


////////////////////////////////////////////////////////////////

module mux(
a,
b,
s,
q
);
input a;
input b;
input s;
output q;
wire q;
assign q=s?b:a;
endmodule


//////////////////////////////////////////////////////////////////

module carry_select(a,b,cin,sum,co,en
);
input [3:0]a;
input [3:0]b;
input cin;
input en;
output [3:0]sum;
output co;
wire [3:0]sum;
wire co,en;
wire s1,c1,s2,c2,s3,c3,s4,s11,s44,c4,c11,s22,c22,s33,c33,c44;

    //assuming carry in 0
    fa x1(a[0],b[0],0,s1,c1,en);
    fa x2(a[1],b[1],c1,s2,c2,en);
    fa x3(a[2],b[2],c2,s3,c3,en);
    fa x4(a[3],b[3],c3,s4,c4,en);
    //assuming carry in 1
    fa x5(a[0],b[0],1,s11,c11,en);
    fa x6(a[1],b[1],c11,s22,c22,en);
    fa x7(a[2],b[2],c22,s33,c33,en);
    fa x8(a[3],b[3],c33,s44,c44,en);
    //select either carry 1 or 0 using carry out of FA
    //mux for sum select
    mux x9(s1,s11,cin,sum[0]);
    mux x10(s2,s22,cin,sum[1]);
    mux x11(s3,s33,cin,sum[2]);
    mux x12(s4,s44,cin,sum[3]);
    //mux for carry select
    mux x13(c4,c44,cin,co);

 endmodule


////////////////////////////////////////////////////////////////
module LP_Adder(  input wire [3:0] in_1, input wire [3:0] in_2, output reg [4:0] Ctl=0,output reg [3:0] out);



reg x;

integer i,j,k;
always@(*)
begin
    for (i=3;i>=0;i=i-1)
    begin
        x=in_1[i]&in_2[i];
        Ctl[i]=Ctl[i+1]|x;
        if(Ctl[i]==0)
            out[i]=in_1[i]^in_2[i];
        else
            out[i]=1'b1;
    end
    /*for (j=0;j>=0;j=j-4)
    begin
        for (k=j;k>j-4;k=k-1)
        begin
            if (k==j)
            begin
                x=in_1[k]&in_2[k];
                Ctl[k]=Ctl[k+4]|Ctl[k+1]|x;
            end
            else
            begin
                x=in_1[k]&in_2[k];
                Ctl[k]=Ctl[k+1]|x;
            end
            if(Ctl[k]==0)
                out[k]=in_1[k]^in_2[k];
            else
                out[k]=1'b1;
        end
    end*/
end
endmodule

////////////////////////////////////////////
module adder_top
#(
parameter Addin=8,
parameter enout=2
)
(
input wire [Addin-1:0]Addin1,
input wire [Addin-1:0]Addin2,
output wire [Addin-1:0]csa_output,
output wire [Addin/2-1:0]error_out
);

wire [3:0]add_output;

wire [enout-1:0]carry;

wire [enout-1:0] en;

assign en=4'b1111;

carry_select csa1 (Addin1[7:4], Addin2[7:4], 0, add_output,carry[0],en[0]);
LP_Adder error_adder(Addin1[3:0],Addin2[3:0],ctl,error_out);
assign  csa_output={add_output,error_out};




endmodule













