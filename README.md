# README #

The repository contains the HDL implementation of Multiplier and Adder units which employs a truncation logic to increase the efficiency of processing units for image processing applications.

Documents: 
    1. 8, 16, 32, 64 bit adder and multiplier units with and without truncation logics. 
    2. Effect of these error adders in image processing application
    3. Final report on the research